package sample;

import javax.annotation.processing.Generated;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-10-16T00:44:18+0000",
    comments = "version: 1.5.5.Final, compiler: Eclipse JDT (IDE) 3.35.0.v20230814-2020, environment: Java 17.0.8 (Azul Systems, Inc.)"
)
public class SourceMapperImpl implements SourceMapper {

    @Override
    public Destination toDestination(Source src) {
        if ( src == null ) {
            return null;
        }

        Destination destination = new Destination();

        destination.setId( src.getId() );
        destination.setName( src.getName() );

        return destination;
    }
}
