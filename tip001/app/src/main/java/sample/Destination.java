package sample;

import lombok.Data;

@Data
public class Destination {
    private int id;
    private String name;
}
