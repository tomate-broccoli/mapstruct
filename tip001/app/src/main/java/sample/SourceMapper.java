package sample;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper
public interface SourceMapper{
    // @Mappimplementation ing(target = "id", source = "id")
    Destination toDestination(Source src);
}
